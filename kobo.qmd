# Kobotoolbox data analysis {#sec-kobo}

```{r}
#| results: "asis"
#| echo: false
source("_common.R")
```



# robotoolbox

`robotoolbox` is an R client to access data from the [`KoboToolbox`](https://www.kobotoolbox.org).

`robotoolbox` is built around [`KoboToolbox API v2`](https://support.kobotoolbox.org/api.html) and its main goal is to ease the process by which you access your collected data.

You will need to set your API token and specify your `KoboToolbox` server URL. An API token is a unique identifier, just like a password. It allows the user to authenticate to `Kobotoolbox` APIs to access your data. The easiest way to set up `robotoolbox` is to store the API token and the server url in your `.Renviron`.

We are using the following environment variables `KOBOTOOLBOX_URL` and `KOBOTOOLBOX_TOKEN` for the url and the token. You can use the `usethis` package and the `usethis::edit_r_environ()` to add the following variables to your `.Renviron` file.

```{r}
#| eval: false
KOBOTOOLBOX_URL="https://kobo.unhcr.org/"
KOBOTOOLBOX_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxx
```

You can get your `token` manually through the `Kobotoolbox` web interface in your account settings. There is also a `kobo_token` function to do the same thing automagically from `R`.

```{r}
#| eval: false
token <- kobo_token(username = "cool_user_name",
                    password = "gRe@TP@$$WoRd",
                    overwrite = TRUE)
```


This token can then be used in the setup through `kobo_setup` or in your `.Renviron`.

```{r}
#| eval: false
kobo_setup(url = "https://kobo.unhcr.org",
           token = token)
```
